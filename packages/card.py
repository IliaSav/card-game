from __future__ import absolute_import

import random
import sys


class Card:
    def __init__(self, rank, suit, hard, soft):
        self.suit = suit
        self.rank = rank
        self.hard = hard
        self.soft = soft

    def __repr__(self):
        return "{__class__.__name__}(suit={suit!r}, rank={rank!r})".\
            format(__class__=self.__class__, **self.__dict__)

    def __str__(self):
        return "{rank}{suit}".format(**self.__dict__)

    def __format__(self, format_spec):
        #print( "Dealer Has {0:%r of %s}".format( hand.dealer_card) )
        if format_spec == "":
            return str(self)
        rs = format_spec.replace("%r", self.rank).\
            replace("%s", self.suit)
        rs = rs.replace("%%", "%")
        return rs

    def __eq__(self, other):
        return self.suit == other.suit and self.rank == other.rank

    def __hash__(self):
        return hash(self.suit)^hash(self.rank)

    def __bytes__(self):
        class_code = self.__class__.__name__[0]
        rank_number_str = {'A': '1', 'J': '11', 'Q': '12', 'K': '13'}.\
            get(self.rank,self.rank)
        string = "("+" ".join([class_code, rank_number_str, self.suit])+")"
        return bytes(string, encoding="utf8")

    def __lt__(self, other):
        #@TODO Illia, update lt,le,gt,ge and so on... to work with other classes
        if not isinstance(other, Card):
            return NotImplemented
        return self.rank < other.rank

    def __le__(self, other):
        try:
            return self.rank <= other.rank
        except AttributeError:
            return NotImplemented

    def __gt__(self, other):
        if not isinstance(other, Card):
            return NotImplemented
        return self.rank > other.rank

    def __ge__(self, other):
        if not isinstance(other, Card):
            return NotImplemented
        return self.rank >= other.rank


class NumberCard(Card):
    def __init__(self, rank, suit):
        super().__init__(str(rank), suit, rank, rank)

    def _points(self):
        return int(self.rank), int(self.rank)


class AceCard(Card):
    def __init__(self, rank, suit):
        super().__init__("A", suit, 1, 11)


class FaceCard(Card):
    def __init__(self, rank, suit):
        super().__init__({11: 'J', 12: 'Q', 13: 'K'}[rank], suit, 10, 10)


#♠
#cards = [AceCard('A','♠'), NumberCard('2','♠')]
#let's create class we'll use to build four manifest constants

class Suit:
    def __init__(self, name, symbol):
        self.name = name
        self.symbol = symbol

    def __str__(self):
        # @TODO UnicodeEncodeError 'charmap" codec can't encode '\u2666'
        # while return self.symbol
        return self.name

Club, Diamond, Heart, Spade = Suit('Club', '♣'), Suit('Diamond', '♦'),\
                              Suit('Heart', '♥'), Suit('Spade', '♠')


def card(rank, suit):
    """Factory function for our various Card subclasses"""
    if rank == 1:
        return AceCard(rank, suit)
    elif 2 <= rank < 11:
        return NumberCard(rank, suit)
    elif 11 <= rank < 14:
        return FaceCard(rank, suit)
    else:
        raise Exception("Rank out of range")


class CardFactory:
    """Card factory class"""
    def rank(self, rank):
        self.class_, self.rank_str = {
            1: (AceCard,'A'),
            11: (FaceCard, 'J'),
            12: (FaceCard, 'Q'),
            13: (FaceCard, 'K'),
        }.get(rank, (NumberCard, str(rank)))
        return self

    def suit(self, suit):
        return self.class_(self.rank_str, suit)


class Deck(list):
    """Multiple 52-card decks in Deck."""
    def __init__(self, decks=1):
        # super().__init__()
        # for i in range(decks):
        #     self.extend(card(r+1, s) for r in range(13)
        #                 for s in (Club, Diamond, Heart, Spade))
        super().__init__(card(r+1, s) for r in range(13)
                         for s in (Club, Diamond, Heart, Spade)
                         for d in range(decks))
        random.shuffle(self)
        burn = random.randint(1, 52)
        for i in range(burn): self.pop()

    def __bool__(self):
        return super().__bool__(self)