import sys


class Hand:
    def __init__(self, dealer_card, *cards):
        self.dealer_card = dealer_card
        self._cards = list(cards)

    # def hard_total(self):
    #     return sum(c.hard for c in self.cards)
    #
    # def soft_total(self):
    #     return sum(c.soft for c in self.cards)
    #
    # @property
    # def total(self):
    #     delta_soft = max(c.soft-c.hard for c in self.cards)
    #     hard = self.hard_total()
    #     if hard+delta_soft <= 21:
    #         return hard+delta_soft
    #     return hard
    #
    # @staticmethod
    # def freeze(other):
    #     hand = Hand(other.dealer_card, *other.cards)
    #     return hand
    #
    # @staticmethod
    # def split(other, card0, card1):
    #     hand0 = Hand(other.dealer_card, other.cards[0], card0)
    #     hand1 = Hand(other.dealer_card, other.cards[1], card1)
    #     return hand0, hand1

    def __str__(self):
        return ", ".join( map(str, self._cards) )

    def __repr__(self):
        return "{__class__.__name__}({dealer_card!r}, {_cards_str})".\
            format(__class__=self.__class__,_cards_str=", ".join( map(repr, self._cards) ),
                   **self.__dict__ )

    def __format__(self, format_specification):
        if format_specification == "":
            return str(self)
        return ", ".join("{0:{fs}}".format(c, fs=format_specification)
                          for c in self._cards )

    # def __eq__(self, other):
    #     if isinstance(other,int):
    #         return self.total() == other
    #     try:
    #         return self.cards == other.cards \
    #             and self.dealer_card == other.dealer_card
    #     except AttributeError:
    #         return NotImplemented
    #
    # def __lt__(self, other):
    #     if isinstance(other,int):
    #         return self.total() < other
    #     try:
    #         return self.cards < other.cards
    #     except AttributeError:
    #         return NotImplemented
    #
    # def __le__(self, other):
    #     if isinstance(other,int):
    #         return self.total() <= other
    #     try:
    #         return self.cards <= other.cards
    #     except AttributeError:
    #         return NotImplemented

    __hash__ = None


class FrozenHand(Hand):
    def __init__(self, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Hand):
            # Clone a hand.
            other = args[0]
            self.dealer_card = other.dealer_card
            self.cards = other.cards
        else:
            # Build a fresh hand.
            super().__init__(*args, **kwargs)

    def __hash__(self):
        h = 0
        for c in self.cards:
            h = (h+hash(c)) % sys.hash_info.modulus
        return h


class HandLazy(Hand):
    @property
    def total(self):
        delta_soft = max(c.soft - c.hard for c in self._cards)
        hard_total = sum(c.hard for c in self._cards)
        if hard_total+delta_soft <= 21:
            return hard_total+delta_soft
        return hard_total

    @property
    def card(self):
        return self._cards

    @card.setter
    def card(self, aCard):
        self._cards.append(aCard)

    @card.deleter
    def card(self):
        self._cards.pop(-1)


class HandEager(Hand):
    def __init__(self, dealer_card, *cards):
        self.dealer_card = dealer_card
        self.total = 0
        self._delta_soft = 0
        self._hard_total = 0
        self._cards = list()
        for c in cards:
            self.card = c

    @property
    def card(self):
        return self._cards

    @card.setter
    def card(self, aCard):
        self._cards.append(aCard)
        self._delta_soft = max(aCard.soft - aCard.hard,self._delta_soft)
        self._hard_total += aCard.hard
        self._set_total()

    @card.deleter
    def card(self):
        removed = self._cards.pop(-1)
        self._hard_total -= removed.hard
        #was this the only ace?
        self._delta_soft = max(c.soft-c.hard for c in self._cards)
        self._set_total()

    def _set_total(self):
        if self._hard_total+self._delta_soft <= 21:
            self.total = self._hard_total+self._delta_soft
        else:
            self.total = self._hard_total

    def split(self, deck):
        """Update current hand and return new."""
        assert self._cards[0].rank == self._cards[1].rank
        c1 = self._cards[-1]
        del self.card
        self.card = deck.pop()
        hand_new = self.__class__(self.dealer_card, c1, deck.pop())
        return hand_new
