from abc import ABCMeta, abstractmethod

from .card import Deck
from .hand import Hand


#Flyweight design pattern
class GameStrategy:
    def insurance(self, hand):
        return False

    def split(self, hand):
        return False

    def double(self,hand):
        return False

    def hit(self, hand):
        # Why not to use Hand class methods?
        # return hand.hard_total()<=17
        return sum(c.hard for c in hand.cards)<=17


class Table:
    """Table class requires some sequence of events by Player instances"""
    def __init__(self):
        self.deck = Deck()

    def place_bet(self, amount):
        print("Bet", amount)

    def get_hand(self):
        try:
            self.hand = Hand(self.deck.pop(), self.deck.pop(), self.deck.pop())
            self.hole_card = self.deck.pop()
        except IndexError:
            # Out of cards: need to shuffle.
            self.deck = Deck()
            return self.get_hand()
        print("Deal ", self.hand)
        return self.hand

    def can_insure(self, hand):
        return hand.dealer_card.insure


class BettingStrategy(metaclass=ABCMeta):
    @abstractmethod
    def bet(self):
        return 1

    def record_win(self):
        pass

    def record_loss(self):
        pass


class Flat(BettingStrategy):
    def bet(self):
        return 1


class Player:
    def __init__(self,table, bet_strategy, game_strategy, **kw):
        """
        Creates a new player associated with a table, and
        configure with proper betting and play strategies.

        :param table: an instance of :class:'Table'
        :param bet_strategy: an instance of :class:`BettingStrategy`
        :param game_strategy: an instance of :class:`GameStrategy`
        """
        self.bet_strategy = bet_strategy
        self.game_strategy = game_strategy
        self.table = table
        self.__dict__.update(kw)

    def game(self):
        self.table.place_bet(self.bet_strategy.bet())
        self.hand = self.table.get_hand()
        if self.table.can_insure(self.hand):
            if self.game_strategy.insurance(self.hand):
                self.table.insure(self.bet_strategy.bet())
