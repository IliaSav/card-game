from __future__ import absolute_import


import os
import sys

from collections import defaultdict


def load_parent_module(dir_name):
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.sys.path.insert(1, parent_dir)
    mod = __import__(dir_name)
    sys.modules[dir_name] = mod


def init_cards():
    cards = [ AceCard('A', Spade), NumberCard('2', Spade), NumberCard('3', Spade)]
    return cards


def init_deck():
    return [card(rank, suit) for rank in range(1,14)
            for suit in (Club, Diamond, Heart, Spade)]


def init_deck_from_factory_class():
    cards = CardFactory()
    decks = [cards.rank(r+1).suit(s) for r in range(13)
             for s in (Club, Diamond, Heart, Spade)]
    return decks


def card_from_bytes(buffer):
    """Create Card Object from bytes."""
    string = buffer.decode("utf8")
    assert string[0] == "(" and string[-1] == ")"
    code, rank_number, suit = string[1:-1].split()
    class_ = {'A': AceCard, 'N': NumberCard, 'F': FaceCard}[code]
    return class_(int(rank_number), suit)


if __name__ == '__main__':
    #relative import
    PARENT_MODULE = 'Card Game'
    load_parent_module(PARENT_MODULE)
    __package__ = PARENT_MODULE

    from .packages.card import *
    from .packages.game import *
    from .packages.hand import *
    cards = init_cards()
    #deck = init_deck()
    #deck_factory = init_deck_from_factory_class()
    status = defaultdict(int)
    d = Deck()
    h = HandLazy(d.pop(), d.pop(), d.pop())
    h1 = HandLazy(d.pop(), d.pop(), d.pop())
    gs = GameStrategy()
    h_f = FrozenHand(h)
    status[h_f] += 1
    #h.cards.append(d.pop())
    print(h)
    print(h.total)
    print(h1)
    print(h1.total)
    print(h > h1)

